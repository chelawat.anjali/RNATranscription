package vapasi;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

public class RnaTest {
    private Rna rnaObj ;

    @BeforeEach
    public void setUp() {
        rnaObj = new Rna();
    }

    @Test
    public void shouldReturnRnaStrand() {
        String dnaStrand = "ACGT";
        String expectedRnaStrand ="UGCA";
        Dna dnaObj = new Dna(dnaStrand);
        String rnaStrand = rnaObj.transcribDnaToRna(dnaObj.getNucleotides());
        assertThat(rnaStrand, is(expectedRnaStrand));
    }

    @Test
    public void shouldCreateDNAObject() {
        String dnaStrand = "ACGT";
        Dna dnaObj = new Dna(dnaStrand);
        boolean actual = (dnaObj == null);
        assertThat(actual, is(false));
    }

    @Test
    public void shouldReturnDNAStrand() {
        String dnaStrand = "ACGT";
        Dna dnaObj = new Dna(dnaStrand);
        assertThat(dnaObj.getNucleotides(),is(dnaStrand));
    }

    @Test
    public void shouldCreateRNAObject() {
        boolean actual = (rnaObj == null);
        assertThat(actual, is(false));
    }

    @Test
    public void shouldReturnRNATranscipForAdenine() {
        String dnaStrand = "A";
        String expectedRnaStrand ="U";
        Dna dnaObj = new Dna(dnaStrand);
        String rnaStrand = rnaObj.transcribDnaToRna(dnaObj.getNucleotides());
        assertThat(rnaStrand, is(expectedRnaStrand));
    }

    @Test
    public void shouldReturnRNATranscipForCytosine() {
        String dnaStrand = "C";
        String expectedRnaStrand ="G";
        Dna dnaObj = new Dna(dnaStrand);
        String rnaStrand = rnaObj.transcribDnaToRna(dnaObj.getNucleotides());
        assertThat(rnaStrand, is(expectedRnaStrand));
    }

    @Test
    public void shouldReturnRNATranscipForGuanine() {
        String dnaStrand = "G";
        String expectedRnaStrand ="C";
        Dna dnaObj = new Dna(dnaStrand);
        String rnaStrand = rnaObj.transcribDnaToRna(dnaObj.getNucleotides());
        assertThat(rnaStrand, is(expectedRnaStrand));
    }

    @Test
    public void shouldReturnRNATranscipForThymine() {
        String dnaStrand = "T";
        String expectedRnaStrand ="A";
        Dna dnaObj = new Dna(dnaStrand);
        String rnaStrand = rnaObj.transcribDnaToRna(dnaObj.getNucleotides());
        assertThat(rnaStrand, is(expectedRnaStrand));
    }
}