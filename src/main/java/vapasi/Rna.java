package vapasi;

import java.util.HashMap;

public class Rna{
    private HashMap<String, String> mapDnaToRnaStrand;

    public Rna(){
        //HashMap- DNA Key to RNA Value
        mapDnaToRnaStrand = new HashMap<String, String>();
        mapDnaToRnaStrand.put("T","A");
        mapDnaToRnaStrand.put("C","G");
        mapDnaToRnaStrand.put("G","C");
        mapDnaToRnaStrand.put("A","U");
    }

    public String transcribDnaToRna(String dnaStrand){
        StringBuilder strandRna = new StringBuilder();
        for(int i = 0 ; i<dnaStrand.length();i++) {
            String strDna = dnaStrand.substring(i,i+1);
            String value = mapDnaToRnaStrand.get(strDna);
            if(value!=null)
               strandRna.append(value);
        }
        return strandRna.toString();
    }
}