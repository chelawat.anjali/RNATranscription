package vapasi;

public class RNAClientProgram {
    public static void main(String[] args) {
        String dnaStrand = "ACGTCCGTA";
        Dna dnaObj = new Dna(dnaStrand);
        Rna rnaObj = new Rna();
        String rnaStrand = rnaObj.transcribDnaToRna(dnaObj.getNucleotides());
        System.out.println(String.format("DNA Strand:%s and RNA Strand:%s",dnaStrand,rnaStrand));
     }
}
